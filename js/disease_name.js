var diseaseNames = {
  "Cancer": [
    "Abraxane",
    "Adriamycin",
    "Cytoxan",
    "Doxil"
  ],
  "Dementia": [
    "Cholinesterase inhibitors",
    "Memantine",
    "Other medications",
    "Occupational therapy"
  ],
  "Diabetes": [
    "Humulin",
    "Novolin",
    "NovoLog",
    "Fiasp",
    "FlexPen"
  ],
  "Heart Disease": [
    "Accupril",
    "Aceon",
    "Benicar HCT",
    "Brilinta"
  ],
  "Hypertension": [
    "Bumetanide",
    "Chlorthalidone",
    "Chlorothiazide",
    "Ethacrynate"
  ]
}