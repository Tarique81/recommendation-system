$(function () {
	// diseae name data fetched from disease_name.js file
	// autocomplete field will show the following values
    $("#dis").autocomplete({
        source: Object.keys(diseaseNames)
    });
});
$(document).ready(function () {
    $('#dis').on('change', function () {
        $('#disname').html(this.value);
    }).change();
    // on selecting search values
    $('#dis').on('autocompleteselect', function (e, ui) {
        $('#disname').html('Recommendation for: ' + ui.item.value);
        // clearing html values for recommendation and prescription
        document.getElementById("recommendation").innerHTML = "";
        document.getElementById("prescription").innerHTML = "";
        // showing medicine list 
        diseaseNames[ui.item.value].forEach(function(medicine){
        	console.log(medicine)
        	$('#recommendation').append("<li class='medi'>"+ medicine +"</li>");
        })
        // putting medicine to prescription list
	    $('li.medi').on('click',function(event){
	    	console.log(event.target.innerHTML)
	    	$('#prescription').append("<p>"+ event.target.innerHTML +"</p>");
	    }) 


    });

});